import {LitElement, html, css} from 'lit-element';
import 'unsplash-img-detail/src/unsplash-img-detail.js';

export class UnsplashImgsResults extends LitElement {
	static get is() {
		return 'unsplash-imgs-results';
	}

	static get properties() {
		return{
			content: {
				type: Object
			}
		};
	}

	static get styles() {
		return css`
		.main-container {
		  padding: 1rem; }

		.img-container {
		  display: flex;
		  flex-direction: row;
		  margin-bottom: 1rem; }

		.img {
		  border-radius: 5px;
		  width: 30%; }

		.img-detail {
		  width: 70%; }
		`;
	}

	constructor() {
		super();
	}

	updated(cp) {
		console.log(this.data);
	}

	render() {
		return html`
		<div class="main-container">
			${this.content ? this.content.data.map(({img_url, title, text}) => {
				return html`
				<div class="img-container">
					<img src="${img_url}" alt="" class="img">
					<unsplash-img-detail class="img-detail" title="${title}" text="${text}"></unsplash-img-detail>
				</div>
				`;
			}) : ``}
		</div>
		`;
	}
}

customElements.define(UnsplashImgsResults.is, UnsplashImgsResults);